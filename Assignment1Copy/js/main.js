//Initialization of API keys
const flickrAPIKey = "18e7885d1f7962a2c2fd7feef695e6b6";
const googleMapsAPIKey = "AIzaSyCZ-8sujZxodx8xW7_VJ-x3k5apBjIKiNw";
const weatherAPIKey = "94998cf875fdb8faea658f0d057cac8f";
//Initialization of API URL calls 
const getSizeRequestURL = `https://www.flickr.com/services/rest/?method=flickr.photos.getSizes&api_key=${flickrAPIKey}&format=json&nojsoncallback=1&photo_id=`;
const searchRequestURL = `https://www.flickr.com/services/rest/?method=flickr.photos.search&api_key=${flickrAPIKey}&per_page=15&sort=relevance&format=json&nojsoncallback=1&text=`;
const weatherRequestURL = `http://api.openweathermap.org/data/2.5/weather?&appid=${weatherAPIKey}&units=metric&q=`;
//Initialization of global variables 
//Photos stores the photos to be displayed in div images-flex in an array
let photos = [];
//Stores the images that have been recently viewed in an array
let recentlyViewed = [];
//The number of requests requested from the API 
let numRequested = 0;
//The number of requests that have ßbeen processed 
let numRecieved = 0;
//Google maps map state
var googleMaps;


/**
 * jQuery function called once document is ready (html loaded), Loads the initial images by searching flickr API for "City"
 * Hides weather info, as weather info is specific to an individual city 
 * Adds click hander to category-item that calls function categorySelect
 * Add click hander to modal-close that calls closeModal
 */
$(document).ready(function () {
    loadInitialImages();
    $("#weather-info").hide();
    $(".category-item").click(categorySelect);
    $("#modal-close").click(closeModal);
    // const fetchPromise = fetch("../data/airports.json");
    // console.log(fetchPromise);


/**
 * Function sets the display of modal-container to none and sents the modal-content src to ""
 * this closes the Modal 
 */
function closeModal() {
    $("#modal-container").css("display", "none");
    $("modal-content").attr("src", "");
}

/**
 * Function Initializes the first instance of the google maps, centing round 15-18 and zoom 2 
 */
function worldMap() {
    googleMaps = new google.maps.Map(document.getElementById('map'), {
        center: { lat: 15, lng: 18 },
        zoom: 2
    });
}

/**
 * Function calls the flickr API search with the input of "Cities", and loops through the length of the photos, 
 * a photoOject is then created with the photo id and photo title. 
 */
function loadInitialImages() {
    let searchQuery = searchRequestURL + "Cities";
    $.get(searchQuery, function (data) {
        numRequested = data.photos.photo.length;
        for (let i = 0; i < data.photos.photo.length; i++) {
            let photoObject = { "id": data.photos.photo[i].id, "title": data.photos.photo[i].title }
            photos.push(photoObject);
            getSizes(photoObject);
        }
    });
}

/**
 * Function shows weather-info at now a category/city has been selected then attaches the selected 
 * category text to the searchQuery, thencategorie, search-input and search button are hidden 
 * to stop the user from excessive inputs. 
 * An API call is then completed using get on searchQuery, and loops through the length of the photos, 
 * a photoOject is then created with the photo id and photo title that is added to the photos array. 
 * The getSizes function is called passing in photoObject, jQuery is used to remove the selected-category 
 * class from all category-items and selected-category class is added to the user selected category. 
 * The title is updated to reflect the user selected title and getCategoryWeatherMap is called passing in 
 * the category selected.
 */
function categorySelect() {
    $("#weather-info").show();
    let category = $(this).text();
    let searchQuery = searchRequestURL + category;
    $("#categories").hide();
    $.get(searchQuery, function (data) {
        numRequested = data.photos.photo.length;
        for (let i = 0; i < data.photos.photo.length; i++) {
            let photoObject = { "id": data.photos.photo[i].id, "title": data.photos.photo[i].title }
            photos.push(photoObject);
            getSizes(photoObject);
        }
    });
    $(".category-item").removeClass("selected-category");
    $(this).addClass("selected-category");
    $("#category-title").html(category)
    getCategoryWeatherMap(category);
}

/**
 * The input to this function is a city/cityItem string
 * The Function comeples an API call on the openmap weather API using the provided city, 
 * then attaches weather info from that API call and places it in a table within the html. 
 * The coordinates are then pulled from the API call and saved to variables, tfinally a new google
 * maps map is generated using the coordinates of the city and a marker is placed on that map.
 */
function getCategoryWeatherMap(cityItem) {
    let weatherQuery = weatherRequestURL + cityItem;
    $.get(weatherQuery, function (data) {
        $("#temp").html(data.main.temp + "°C");
        $("#max-temp").html(data.main.temp_max + " °C");
        $("#min-temp").html(data.main.temp_min + " °C");
        $("#wind-speed").html(data.wind.speed + " km/h");
        var categoryCoordinates = { lat: data.coord.lat, lng: data.coord.lon };
        var googleMaps = new google.maps.Map(document.getElementById('map'), {
            zoom: 8,
            center: categoryCoordinates
        });
        new google.maps.Marker({
            position: categoryCoordinates,
            map: googleMaps,
            title: 'Selected City'
        });
    });
}

/**
 * The function input is a photoObject, this function calls the getSizeQuery using a specific photo ID 
 * then loops through the avaliable sizes and stores the required sizes in variables. 
 * Once the numRequested equals the numRecieved then the display function is called with the photos array.
 * Finally the numRequested,numRecieved and photos are reset to initial values. 
 * (note that display does not actually need to pass global variable photos, however for readability I have kept it in)
 */
function getSizes(photoObject) {
    getSizeQuery = getSizeRequestURL + photoObject.id
    $.get(getSizeQuery, function (data) {
        numRecieved++;
        photoObject.thumbnail = getCorrectSize(data, "Small") ?? data.sizes.size[0].source;
        photoObject.recentlyViewed = getCorrectSize(data, "Large Square") ?? data.sizes.size[0].source;
        photoObject.full = getCorrectSize(data, "Large") ?? data.sizes.size[data.sizes.size.length - 1].source;
        if (numRequested === numRecieved) {
            display(photos);
            numRequested = 0;
            numRecieved = 0;
            photos = [];
        }
    });
}

/**
 * The input to this function is data and selected size, data is the array of avaliable photos and their sizes 
 * selectedSize is the size that is to be selected, e.g "large"
 * The funtion loops through the data and if it finds a match to the size of the photo to the selected size 
 * it returns the source
 */
function getCorrectSize(data, selectedSize) {
    for (let i = 0; i < data.sizes.size.length; i++) {
        if (data.sizes.size[i].label === selectedSize) {
            return data.sizes.size[i].source;
        }
    }
}

/**
 * The inpit to this function is photos, (however variable is global so not necessarily required as its in global space) 
 * I have kept it in for code readability. 
 * The function then loops through the length of photos and adds to htmlStr a sting of the photo in a HTML figure tag with the
 * photoObject information.
 * images-flex is then assigned the htmlStr, the attachModal function is called and finally the categories, search-inpit 
 * and search-button are shown again.
 */
function display(photos) {
    let htmlStr = "";
    for (let i = 0; i < photos.length; i++) {
        htmlStr += `<figure data-full="${photos[i].full}" data-recentlyViewed="${photos[i].recentlyViewed}" data-title="${photos[i].title}"><img src = "${photos[i].thumbnail}" alt="${photos[i].title}" height="200" width="200"><figcaption>${photos[i].title}</figcaption></figure>`;
    }
    $("#images-flex").html(htmlStr);
    attachModal();
    $("#categories").show();
}

/**
 * Function has an inpit of array and objecy, array is the array of recentlyViewed images, and object is the object 
 * to be checked against. 
 * The function then loops through the array, and checks if the object is in the array. 
 * If the object is found its index is returned, otherwise fasle is returned. 
 */
function checkObjectInArray(array, object) {
    for (let i = 0; i < array.length; i++) {
        if (array[i].full === object.full && array[i].title === object.title) {
            return i;
        };
    }
    return false;
}

/**
 * Function is used to render the recently viewed images, first checking that the length of the array is never 
 * greater than 6, if it is the last index of the array is removed. 
 * Then loops though and creates a string containing all the recently viewed photos as figures. 
 * Finally the recently-viewed div is appended with htmlStr  and the attachModal function is called.
 */
function renderRecentlyViewed() {
    let htmlStr = ""
    if (recentlyViewed.length === 6) {
        recentlyViewed.splice(recentlyViewed.length - 1, 1);
    }
    for (let i = 0; i < recentlyViewed.length; i++) {
        htmlStr += `<figure data-full="${recentlyViewed[i].full}" data-recentlyViewed="${recentlyViewed[i].recentlyViewed}" data-title="${recentlyViewed[i].title}"><img src = "${recentlyViewed[i].recentlyViewed}" alt="${recentlyViewed[i].title}" height="140" width="140"><figcaption>${recentlyViewed[i].title}</figcaption></figure>`;
    }
    $("#recently-viewed").html(htmlStr);
    attachModal();

}

/**
 * Function is used to attach the Modal to all figures in the html, by looping though 
 * all figures and adding a click function to that figure Modal details. 
 */
function attachModal() {
    $('figure').each(function (index) {
        $(this).click(function () {
            $("#modal-container").css("display", "block");
            $("#modal-content").attr('src', $(this).attr("data-full"));
            $("#modal-caption").html($(this).attr("data-title"));
            let photoObject = { "full": $(this).attr("data-full"), "recentlyViewed": $(this).attr("data-recentlyViewed"), "title": $(this).attr("data-title") };
            let itemInArray = checkObjectInArray(recentlyViewed, photoObject);
            if (itemInArray === false) {
                recentlyViewed.unshift(photoObject);
            } else {
                recentlyViewed.splice(itemInArray, 1);
                recentlyViewed.unshift(photoObject);
            }
            renderRecentlyViewed();
        })
    });

}