import $ from "jquery"
// import styles from "./css/styles.css"
import img1 from "../src/assets/image.png"
import hello from "./templates/hello.handlebars";
import thumbnails from "./templates/thumbnails.handlebars";

let data = {
        photos: [
            {src: img1, title: "Whatever"},
            {src: img1, title: "Second Title"},
            {src: img1, title: "Third Title"}
        ]}

let name = {loggedin: true,first: "Bob", last: "Hawk"}
$(document).ready(function() {
    $('#photo').html(thumbnails(data));
    let temp = hello(name);    
    console.log(temp);
    $('#hello').html(temp);
});