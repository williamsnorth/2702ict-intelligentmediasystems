import $ from "jquery";

const API_KEY = "18e7885d1f7962a2c2fd7feef695e6b6";
const interestingRequest = "https://www.flickr.com/services/rest/?method=flickr.interestingness.getList&api_key=" + API_KEY + "&per_page=10&format=json&nojsoncallback=1";
const getSizeRequest = "https://www.flickr.com/services/rest/?method=flickr.photos.getSizes&api_key=" + API_KEY + "&format=json&nojsoncallback=1&photo_id=";
const searchRequest = `https://www.flickr.com/services/rest/?method=flickr.photos.search&api_key=${API_KEY}&per_page=10&sort=relevance&format=json&nojsoncallback=1&text=`;
let photos = [];
let nRequests = 0;
let nRecieved = 0;
let ready_cb; 

export function getInteresting(ready) {
    ready_cb = ready;
    $.get(interestingRequest, function (data) {
        fetchPhoto(data);
    });
}

export function searchFlickr(input) {
    if (input != "") {
        let searchQuery = searchRequest + input;
        $.get(searchQuery, function (data) {
            nRequests = data.photos.photo.length;
            for (let i = 0; i < data.photos.photo.length; i++) {
                let photoObject = { "id": data.photos.photo[i].id, "title": data.photos.photo[i].title }
                photos.push(photoObject);
                getSizes(photoObject);
            }
        });
    }
}




function fetchPhoto(data){
    nRequests = data.photos.photo.length;
    for (let i = 0; i < data.photos.photo.length; i++) {
        let photoObject = {"id": data.photos.photo[i].id, "title": data.photos.photo[i].title}
        photos.push(photoObject);
        getSizes(photoObject);
    }
}

function getSizes(photoObject) {
    let getSizeQuery = getSizeRequest + photoObject.id
    $.get(getSizeQuery, function(data){
         nRecieved++;
         photoObject.file = data.sizes.size[4].source;
         photoObject.full = data.sizes.size[data.sizes.size.length -1].source;
         if (nRequests === nRecieved) {
             ready_cb(photos);
             nRequests = 0;
             nRecieved = 0;
             photos = [];
         } 
    }); 
}
