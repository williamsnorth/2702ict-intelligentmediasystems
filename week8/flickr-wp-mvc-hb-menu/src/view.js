import $ from "jquery";
import thumbs from "./templates/photos.handlebars"
//import thumbs "./templates/photos.handlebars";
import categories from "./templates/categories.handlebars"

let data = {
    categories: [
        {title: "Nature"},
        {title: "General"},
        {title: "Astrophotography"},
        {title: "Sports"}
    ]}
export function display(photosArray) {
    let data = {photos: photosArray}
    $("#images-flex").html(thumbs(data));
}

export function displayCategories() {
    $("#categories").html(categories(data));
}

export function addCategory(userInput) {
    data.categories.push({title: userInput});
    console.log(data);
}


