import $ from "jquery";
import styles from "../src/css/styles.css";
import { display } from "./view.js";
import * as flickr from "./flickr.js";
import { bootstrap } from "./facebook.js";
import { processButton } from "./facebook.js";



$(document).ready(function () {
    // bootstrap();
    // $("#button").click(function(){
    //     processButton()
    // });
    flickr.getInteresting(flickrReady); 
    $("#search-button").click(seachButton);
    $("#modal-close").click(function(){
        $("#modal-container").css("display", "none");
        $("modal-content").attr("src","");
    });

});



function flickrReady(photos){
    display(photos);
    modalRegister();
}

function seachButton() {
    let userInput = $("#search-input").val();
    flickr.searchFlickr(userInput);
}

function modalRegister() {
    $('figure').each(function(index) {
        $(this).click(function(){
            $("#modal-container").css("display", "block");
            $("#modal-content").attr('src', $(this).attr("data-full"));
            $("#modal-caption").html($(this).attr("data-title"));
        })
    });
}

