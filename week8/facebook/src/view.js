import $ from "jquery";
import thumbs from "./templates/photos.handlebars"
//import thumbs "./templates/photos.handlebars";
import button from "./templates/button.handlebars";
import name from "./templates/name.handlebars";


export function display(photosArray) {
    let data = {photos: photosArray}
    $("#images-flex").html(thumbs(data));

}


export function renderUserLogin(username) {
    $("#button").html(button({loggedin: true}))
    // $("#name").html(name({name: name}))
    //$("#name").html(`<h1>${name}</h1>`)
    let data = {name:username}
    $("#name").html(name(data))
}

export function renderUserLogout() {
    $("#button").html(button({loggedin: false}))
    let data = {name:undefined}
    $("#name").html(name(data))
}




