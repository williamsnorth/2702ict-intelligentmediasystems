import $ from "jquery";
import { renderUserLogin } from "./view.js"
import { renderUserLogout } from "./view.js"
const APP_ID = "610960562964190";


let userloggedIn = false

export function bootstrap() {
    console.log("in bootstrap")
    $.ajaxSetup({ cache: true });
    $.getScript('https://connect.facebook.net/en_US/sdk.js', function(){
      FB.init({
        appId: APP_ID,
        version: 'v6.0' // or v2.1, v2.2, v2.3, ...
      });     
      //FB.getLoginStatus(updateStatusCallback);
    });   
}
// function updateStatusCallback(response) {
//     if (response.status === 'connected') {
//         // The user is logged in and has authenticated your
//         // app, and response.authResponse supplies
//         // the user's ID, a valid access token, a signed
//         // request, and the time the access token 
//         // and signed request each expire.
//         console.log("Connected");
//         var uid = response.authResponse.userID;
//         var accessToken = response.authResponse.accessToken;
//       } else if (response.status === 'not_authorized') {
//         console.log("Not Authorized");
//         // The user hasn't authorized your application.  They
//         // must click the Login button, or you must call FB.login
//         // in response to a user gesture, to launch a login dialog.
//       } else {
//         console.log("User not logged in to facebook");
//         // The user isn't logged in to Facebook. You can launch a
//         // login dialog with a user gesture, but the user may have
//         // to log in to Facebook before authorizing your application.
//       }
// }

export function login() {
  FB.login(function(response){
    FB.api('./me',function(response) {
      renderUserLogin(response.name);
      userloggedIn = true;
    });
  }, {scope: 'email'});

}

export function logout() {
  FB.logout(function(response){
    renderUserLogout(response.name);
    userloggedIn = false;

  })
}

export function processButton() {
  console.log(userloggedIn);
  if (userloggedIn === true) {
    logout();
  } else {
    login();
  }
}