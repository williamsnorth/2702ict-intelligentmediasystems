import $ from "jquery";
import thumbs from "./templates/photos.handlebars"
//import thumbs "./templates/photos.handlebars";
import button from "./templates/button.handlebars";
import name from "./templates/name.handlebars";
import userData from "./templates/userData.handlebars";

export function display(photosArray) {
    let data = {photos: photosArray}
    $("#images-flex").html(thumbs(data));

}


export function renderUserLogin(username) {
    console.log(username);
    $("#button").html(button({loggedin: true}))
    let data = {name:username}
    $("#name").html(name(data))
}

export function renderUserLogout() {
    $("#button").html(button({loggedin: false}))
    let data = {name:undefined}
    $("#name").html(name(data))
}

export function renderUserData(age_range, birthday, posts) {
    console.log(posts);
    let data = {age_range: age_range.min, birthday: birthday, posts: posts.data}
    $("#images-flex").html(userData(data));
}


