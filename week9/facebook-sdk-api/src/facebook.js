import $ from "jquery";
const APP_ID = "610960562964190";


export function bootstrap() {
    console.log("in bootstrap")
    $.ajaxSetup({ cache: true });
    $.getScript('https://connect.facebook.net/en_US/sdk.js', function(){
      FB.init({
        appId: APP_ID,
        version: 'v6.0' // or v2.1, v2.2, v2.3, ...
      });     
      //FB.getLoginStatus(updateStatusCallback);
    });   
}

0
export function login(setUserLogin,setRenderData) {
  FB.login(function(response){
    FB.api('me?fields=id,name,email,location,gender,birthday,address,hometown',function(response) {
      setUserLogin(response.name);
      getUserData(setRenderData);

    });
  }, {scope: ['email', 'user_location', 'user_birthday', 'user_posts','user_age_range','user_gender']});

}

export function logout(setUserLogout) {
  FB.logout(function(response){
    console.log(response);
    setUserLogout();
  })
}

export function processButton(setRenderData,setUserLogin,setUserLogout) {
  FB.getLoginStatus(function(response){
    if (response.status === 'connected') {
      console.log("connected");
      logout(setUserLogout);
    } else if (response.status === 'not_authorized') {
      console.log("Not Authorized");
    } else {
      console.log("Not connected");
      login(setUserLogin,setRenderData);
    }
  })
}


export function getUserData(setRenderData) {
  FB.api('/me','GET', {"fields":"posts,name,id,birthday,age_range"},
    function(response) {
        setRenderData(response.age_range,response.birthday, response.posts);
    }
  );
}