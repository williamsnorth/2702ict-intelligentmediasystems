import $ from "jquery";
import styles from "../src/css/styles.css";
import { display } from "./view.js";
import * as flickr from "./flickr.js";
import { bootstrap } from "./facebook.js";
import { processButton } from "./facebook.js";
import { getUserData } from "./facebook.js";
import * as view from "./view.js";


$(document).ready(function () {
    bootstrap();
    $("#button").click(function(){
        processButton(setRenderData,setUserLogin,setUserLogout);
    });
    // $("#search-button").click(function(){
    //     getUserData();
    // });
    //flickr.getInteresting(flickrReady); 
    // $("#search-button").click(seachButton);
    $("#modal-close").click(function(){
        $("#modal-container").css("display", "none");
        $("modal-content").attr("src","");
    });
});

function flickrReady(photos){
    display(photos);
    modalRegister();
}

function seachButton() {
    let userInput = $("#search-input").val();
    flickr.searchFlickr(userInput);
}

function modalRegister() {
    $('figure').each(function(index) {
        $(this).click(function(){
            $("#modal-container").css("display", "block");
            $("#modal-content").attr('src', $(this).attr("data-full"));
            $("#modal-caption").html($(this).attr("data-title"));
        })
    });
}


export function setUserLogout() {
    view.renderUserLogout();
}

export function setUserLogin(name) {
    view.renderUserLogin(name);
}

export function setRenderData(age_range,birthday, posts) {
    view.renderUserData(age_range,birthday, posts);
}