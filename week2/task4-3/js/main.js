let fig;

$(document).ready(function(){
    randomImage();
    fig = $("figure").html();
    $("#search-button").click(generateuserInput);

});

function generateuserInput() {
        let userInput = $("#search-input").val();
        let htmlStr = "";
        if (isNaN(userInput)) {
            $("#categories").append(`<li><a href="https://www.google.com">${userInput}</a></li>`);
        } else {
            for (let i = 0; i < userInput; i++) {
                htmlStr += `<figure>${fig}</figure>`
            }
            $("#images-flex").html(htmlStr);
        }
}


function randomImage() {
    let fig = $("figure").html();
    let randomNum = Math.floor(15 * Math.random() + 1); 
    let htmlStr = "";
    for (let i = 0; i < randomNum; i++) {
        htmlStr += `<figure>${fig}</figure>`
    }
    $("#images-flex").html(htmlStr);
}

