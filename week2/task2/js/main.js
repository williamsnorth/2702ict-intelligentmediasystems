const fig = document.getElementsByTagName("figure")[0].innerHTML;

function generateRandomImages() {
    document.getElementById("images-flex").innerHTML = "";
    const randomNum = Math.floor(15 * Math.random() + 1); 
    for (let i = 0; i < randomNum; i++) {
        document.getElementById("images-flex").innerHTML += `<figure>${fig}</figure>`;
    }
}

generateRandomImages()