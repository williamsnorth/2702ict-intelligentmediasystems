const fig = document.getElementsByTagName("figure")[0].innerHTML;

function generateuserInput() {
    userInput = document.getElementById("search-input").value;
    if (isNaN(userInput)) {
        console.log("Not a number");
        document.getElementById("categories").innerHTML +=  `<li><a href="https://www.google.com">${userInput}</a></li>`;
    } else {
        document.getElementById("images-flex").innerHTML = "";
        for (let i = 0; i < userInput; i++) {
            document.getElementById("images-flex").innerHTML += `<figure>${fig}</figure>`;
        };
    }
}

function generateRandomImages() {
    document.getElementById("images-flex").innerHTML = "";
    const randomNum = Math.floor(15 * Math.random() + 1); 
    for (let i = 0; i < randomNum; i++) {
        document.getElementById("images-flex").innerHTML += `<figure>${fig}</figure>`;
    }
}

generateRandomImages()  

document.getElementById("search-button").onclick = generateuserInput  

