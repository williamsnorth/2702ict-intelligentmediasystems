let fig;

$(document).ready(function(){
    fig = $("figure").html();
    $("#search-button").click(seachButton);
});

function seachButton() {
        let inputNum = $("#search-input").val();
        let htmlStr = "";
        for (let i = 0; i < inputNum; i++) {
            htmlStr += `<figure>${fig}</figure>`
        }
        $("#images-flex").html(htmlStr);
}
