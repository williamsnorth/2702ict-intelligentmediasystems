const fig = document.getElementsByTagName("figure")[0].innerHTML;

function generateImages() {
    let inputNum = document.getElementById("search-input").value;
    document.getElementById("images-flex").innerHTML = "";
    for (let i = 0; i < inputNum; i++) {
        document.getElementById("images-flex").innerHTML += `<figure>${fig}</figure>`;
    };
};

document.getElementById("search-button").onclick = generateImages();