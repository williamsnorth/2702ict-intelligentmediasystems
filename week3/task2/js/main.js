let fig; 
const API_KEY = "18e7885d1f7962a2c2fd7feef695e6b6"
const interestingRequest = "https://www.flickr.com/services/rest/?method=flickr.interestingness.getList&api_key=" + API_KEY + "&per_page=20&format=json&nojsoncallback=1";

let getSizeRequest = "https://www.flickr.com/services/rest/?method=flickr.photos.getSizes&api_key=" + API_KEY + "&photo_id=";
const format = "&format=json&nojsoncallback=1"

$(document).ready(function(){
    $.get(interestingRequest, function(data){
        fetchPhoto(data);
    });
});

function fetchPhoto(data){
    let photoId = data.photos.photo[0].id
    let title = data.photos.photo[0].title
    getSizes(photoId,title);
}

function getSizes(photoId,title) {
    getSizeRequest = getSizeRequest + photoId + format
    $.get(getSizeRequest, function(data){
        let photo = data.sizes.size[9].source;
        let photoData = [{"file": photo, "title": title}];
        display(photoData);
        //get a sized based on a identifier 
        //let label = data.sizes.size
        // for (i of label) {
        //     if (i.label === "Square") {
        //         console.log("Square found!!")
        //     };
        // }
    }); 
}


function display(photos) {
    let htmlStr = "";
    for (let i = 0; i < photos.length; i++) {
        htmlStr += `<figure><img src = "${photos[i].file}" alt="City view" height="200" width="200"><figcaption>${photos[i].title}</figcaption></figure>`;
    }
    $("#images-flex").html(htmlStr);
}

