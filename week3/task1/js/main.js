let fig; 
$(document).ready(function(){
    $("#images-flex").hide();
    $.get("./data/photodata.json", function(data){
        display(data)
        fig = data.photos[0];
    });
    $("#search-button").click(seachButton);
    $("#images-flex").show(1000);

});

function display(data) {
    let htmlStr = "";
    for (let i = 0; i < data.photos.length; i++) {
        htmlStr += `<figure><img src = "${data.photos[i].file}" alt="City view" height="200" width="200"><figcaption>${data.photos[i].title}</figcaption></figure>`;
    }
    $("#images-flex").html(htmlStr);
}

function seachButton() { 
    let inputNum = $("#search-input").val();
    let htmlStr = "";
    for (let i = 0; i < inputNum; i++) {
        htmlStr += `<figure><img src = "${fig.file}" alt="City view" height="200" width="200"><figcaption>${fig.title}</figcaption></figure>`;
    }
    $("#images-flex").html(htmlStr);
}




