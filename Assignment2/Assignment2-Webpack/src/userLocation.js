//Users latitude value
let userLatitude = "";
//Users longitude valie
let userLongitude = "";

//Function used to access the users location using geolocation, then saves the postion
//to userLatitude and userLongitude values.
export function getUserLocation(renderLocationPrivate, renderLocationShared) {
  if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(function (position) {
      userLatitude = position.coords.latitude;
      userLongitude = position.coords.longitude;
      renderLocationShared(userLatitude, userLongitude);
    });
  } else {
    console.log("no navigator geolocation avaliabe")
    renderLocationPrivate();
  }
}

//Function used to return the users lat and long 
export function getUserCoordinates() {
  return [userLatitude, userLongitude]
}

