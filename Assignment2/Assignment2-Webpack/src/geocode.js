//Function stores the users hometown coordinates that have been geocoded by the API 
let userCoordainates = []; 

//Function used to foward geocode a location based on a name input. 
//The function saves the coodinates of the result in the userCoordinates function 
export function fowardGeocode(placeName) { 
    let search = `https://s5062803.elf.ict.griffith.edu.au:3001/?placename=${placeName}`
    fetch(search).then(response => {
        return response.json();
    }).then(json => {
        userCoordainates = [json.results[0].geometry.lat,json.results[0].geometry.lng]
    })
} 

//Function used to return the local userCoordinates variable 
export function getUserCoordaintes(){
    return userCoordainates;
}