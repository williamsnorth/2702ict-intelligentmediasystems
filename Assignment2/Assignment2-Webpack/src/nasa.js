
//Function uses a lat and long value to create an array of fetchURLs with varying dates for the NASA API look up.
//Function then uses a promise all to return all the JSONS of the URLs and adding them to an image object
//image objects + coordinates are then sent to the getWeatherand displayImages
export function getImageGroup(lat, long, displayImages, getWeather, modalRegister, renderWeatherInfo) {
    let imageObjects = [];
    let fetchURLs = [];
    let dates = ["2014-01-01", "2015-06-06", "2017-01-01", "2018-06-06", "2020-01-01"]  //1.5 year gap
    //0.4
    for (let i = 0; i < dates.length; i++) {
        fetchURLs.push(`https://s5062803.elf.ict.griffith.edu.au:3001/?date=${dates[i]}&lat=${lat}&long=${long}`);
    }

    let fetchPromises = fetchURLs.map(url => fetch(url));
    Promise.all(fetchPromises).then(responses => {
        return Promise.all(responses.map(resp => resp.json()));
    }).then(jsons => {
        for (let i = 0; i < jsons.length; i++) {
            let imageObject = { date: dates[i], url: jsons[i].url }
            imageObjects.push(imageObject);
        }
        getWeather(lat, long, renderWeatherInfo);
        displayImages(imageObjects, modalRegister);

    });
}

//Function uses a lat and long value to fetch an image from the NASA API look up.
//Then image data is stored in an object with its coordinates then sent to the getWeatherand displayImages
export function getImageUserLocation(userCoordinates, date, getWeather, displayUserImage, modalRegister, renderWeatherInfo) {
    let userLatitude = userCoordinates[0];
    let userLongitude = userCoordinates[1];
    let imageObject;
    fetch(`https://s5062803.elf.ict.griffith.edu.au:3001/?date=${date}&lat=${userLatitude}&long=${userLongitude}`).then(response => {
        return response.json();
    }).then(json => {
        imageObject = { date: date, url: json.url }
        getWeather(userLatitude, userLongitude, renderWeatherInfo);
        displayUserImage(imageObject, modalRegister);
    });
}