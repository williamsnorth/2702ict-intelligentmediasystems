import $ from "jquery";
import styles from "../src/css/styles.css";
import * as googleMaps from "./googleMaps.js";
import * as facebook from "./facebook.js";
import * as view from "./view.js";
import * as nasa from "./nasa.js";
import * as userLocation from "./userLocation.js";
import * as geocode from "./geocode.js";
import * as openWeather from "./openWeather.js";

//Document ready for assigning variables once the HTML has loaded 
$(document).ready(function () {
    view.hideLoadingAnimation();
    googleMaps.initializeGoogleMaps(mapEventListener);
    facebook.bootstrap();
    userLocation.getUserLocation(view.renderLocationPrivate, view.renderLocationShared, modalRegister);
    setFacebookButton();
    setHometownSearch();
    setLocationSearch();
    setModelClose();
});

//Function adds the facebook.processButton function to the facebook-login id (button)
function setFacebookButton() {
    $("#facebook-login").click(function () {
        facebook.processButton(view.renderFacebookLogout, view.renderFacebookLogin, geocode.fowardGeocode);
    });
}

//Sets the click function on the hometown search to the hometown id, date gets todays current date which is used to get the latest images from the nasa API
function setHometownSearch() {
    $("#hometown").click(function () {
        view.showLoadingAnimation();
        let hometownCoords = geocode.getUserCoordaintes();
        let today = new Date();
        let date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();
        nasa.getImageUserLocation(hometownCoords, date, openWeather.getWeather, view.displayUserImage, modalRegister, view.renderWeatherInfo);
    });
}

//Sets the location search on the userlocation id to search the NASA API with the users coordinates, date gets the current date to use in the NASA API
function setLocationSearch() {
    $("#userlocation").click(function () {
        view.showLoadingAnimation();
        let userCoordinates = userLocation.getUserCoordinates();
        let today = new Date();
        let date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();
        nasa.getImageUserLocation(userCoordinates, date, openWeather.getWeather, view.displayUserImage, modalRegister, view.renderWeatherInfo);
    });
}

//Adds click function to the modal close button then calls the view to render to hide the CSS/HTML
function setModelClose() {
    $("#modal-close").click(function () {
        view.renderCloseModel();
    });
}

//Function registers each figure with a click function that calls the view.displayModal to show the figure modal
export function modalRegister() {
    $('figure').each(function (index) {
        $(this).click(function () {
            view.displayModal(this);
        });
    });
}

//Function adds a click event listener to process the NASA API request of the users selected location
function mapEventListener(map, infoWindow) {
    map.addListener('click', function (mapsMouseEvent) {
        view.showLoadingAnimation();
        infoWindow.close();
        googleMaps.processMapPress(map, infoWindow, view.displayImages, nasa.getImageGroup, openWeather.getWeather, modalRegister, mapsMouseEvent, view.renderWeatherInfo);
    });
}
