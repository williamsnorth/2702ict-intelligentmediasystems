//Function used to set the inital google maps infowindow and coordinates 
export function initMap(mapEventListener) {
    let myLatlng = { lat: -25, lng: 131 };
    let map = new google.maps.Map(
        document.getElementById('map'), { zoom: 4, center: myLatlng });
    let infoWindow = new google.maps.InfoWindow(
        { content: '<h6>Click the map to select a location</h6>', position: myLatlng });
    infoWindow.open(map);
    mapEventListener(map, infoWindow);
}

//Function used to access the google maps API and add the script to the app 
export function initializeGoogleMaps(mapEventListener) {
    var script = document.createElement('script');
    script.src = 'https://maps.googleapis.com/maps/api/js?key=AIzaSyCZ-8sujZxodx8xW7_VJ-x3k5apBjIKiNw&callback=initMap';
    script.defer = true;
    script.async = true;
    window.initMap = function () {
        initMap(mapEventListener);
    };
    document.head.appendChild(script);
}

//Function used to process a user press on the map, accesses the coodinates of th map click then passes 
//the lat + long to the NASA API along with additional functions for rendering the data and accessing weather
export function processMapPress(map, infoWindow, displayImages, getImageGroup, getWeather, modalRegister, mapsMouseEvent, renderWeatherInfo) {
    let str = mapsMouseEvent.latLng.toString()
    let array = str.split(",");
    let lat = array[0].substr(1, array[0].length - 1)
    let long = array[1].substr(1, array[1].length - 2)
    getImageGroup(lat, long, displayImages, getWeather, modalRegister, renderWeatherInfo);
    infoWindow = new google.maps.InfoWindow({ position: mapsMouseEvent.latLng });
    infoWindow.setContent(mapsMouseEvent.latLng.toString());
    infoWindow.open(map);
}
