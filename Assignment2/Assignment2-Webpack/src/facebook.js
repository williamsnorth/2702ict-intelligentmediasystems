import $ from "jquery";
const APP_ID = "610960562964190";

//Function used to setup the facebook SDK authentication 
export function bootstrap() {
  $.ajaxSetup({ cache: true });
  $.getScript('https://connect.facebook.net/en_US/sdk.js', function () {
    FB.init({
      appId: APP_ID,
      version: 'v6.0'
    });
  });
}

//Function used to authenticate the user and log into facebook
//If the response is sucessful passes the response hometown to the fowardGeocode function and 
//calls the renderFacebook login to update the UI
function login(renderFacebookLogin, fowardGeocode) {
  FB.login(function (response) {
    FB.api('me?fields=hometown', function (response) {
      renderFacebookLogin(response.hometown.name);
      fowardGeocode(response.hometown.name);
    });
  }, { scope: [] });
}

//Function used to log the user out of facebook
//Upon sucessful response the function calls renderFacebookLogout to update the UI 
function logout(renderFacebookLogout) {
  FB.logout(function (response) {
    renderFacebookLogout();
  })
}

//Function used to process the users click on the login/logout button 
//if the user is currently authenticated with facebook the user is 
//logout function is called. If the response is not_authorized then 
//the function logs the error in console. Else the login function is called 
export function processButton(renderFacebookLogout, renderFacebookLogin, fowardGeocode) {
  FB.getLoginStatus(function (response) {
    if (response.status === 'connected') {
      logout(renderFacebookLogout);
    } else if (response.status === 'not_authorized') {
      console.log("Not Authorized");
    } else {
      login(renderFacebookLogin, fowardGeocode);
    }
  })
}