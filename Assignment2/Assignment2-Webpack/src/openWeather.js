//Function used to fetch data from the proxy and return a JSON with the weather data. 
//data is then passed to the renderWeatherInfo function to display the data 
export function getWeather(lat, long, renderWeatherInfo) {
    let search = `https://s5062803.elf.ict.griffith.edu.au:3001/?lat=${lat}&long=${long}`;
    fetch(search).then(response => {
        return response.json();
    }).then(data => {
        renderWeatherInfo(data);
    });
}