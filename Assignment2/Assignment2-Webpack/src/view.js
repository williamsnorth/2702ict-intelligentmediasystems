import $ from "jquery";
import facebookButton from "./templates/facebookButton.handlebars";
import displayhometown from "./templates/displayhometown.handlebars";
import displayuserlocation from "./templates/displayuserlocation.handlebars";
import photos from "./templates/photos.handlebars";
import photo from "./templates/photo.handlebars";
import displayWeather from "./templates/displayWeather.handlebars";

//Function used to display a group of NASA images in the images-flex div from an array of image 
//objects, then calls the modelRegister function to set the modal
export function displayImages(imgObjects, modalRegister) {
    $("#images-flex").html(photos({ imgObjects: imgObjects }));
    modalRegister();
    hideLoadingAnimation();
}

//Function used to display a single NASA images images-flex div from an array of image 
//object, then calls the modelRegister function to set the modal
export function displayUserImage(imgObject, modalRegister) {
    $("#images-flex").html(photo({ imgObjects: imgObject }));
    modalRegister();
    hideLoadingAnimation();

}

//Function uses the displayuserlocation template with a lat and lng value to display 
//the users coordinates within the userlocation id 
export function renderLocationShared(lat, lng) {
    console.log(lat, lng);
    $("#userlocation").html(displayuserlocation({ lat: lat, lng: lng }))
}

//Renders the user location as private (can not access coordaintes)
export function renderLocationPrivate() {
    $("#userlocation").html(displayuserlocation({}))
}

//Once the user has logged out of facebook function called to set button back to 
//login and remove the users hometown 
export function renderFacebookLogout() {
    console.log("called FBlogout")
    $("#facebook-login").html(facebookButton({ loggedin: false }))
    $("#hometown").html(displayhometown({}))
}

//Function called when the user logs into facebook, loads the users hometown (if avaliable)
//Then loads the logout button
export function renderFacebookLogin(hometown) {
    $("#facebook-login").html(facebookButton({ loggedin: true }))
    if (hometown === undefined) {
        $("#hometown").html(displayhometown({ nohometown: true }))
    }
    $("#hometown").html(displayhometown({ hometown: hometown }))
}

//Function used to remove the modal CSS/HTML
export function renderCloseModel() {
    $("#modal-container").css("display", "none");
    $("modal-content").attr("src", "");
}

//Function displays the weather data using the displayWeather template.
export function renderWeatherInfo(data) {
    $("#weather-info").html(displayWeather({ data: data }));
}

//Function hides the loading animation and shows the images flex + weather info divs
export function hideLoadingAnimation() {
    $(".css-loading").hide();
    $("#images-flex").show();
    $("#weather-info").show();
}

//Function hides the weather-info + images-flex div and shows the CSS loading animation 
export function showLoadingAnimation() {
    $("#weather-info").hide();
    $("#images-flex").hide();
    $(".css-loading").show();
}

//Function adds the CSS attributes for the modal when being shown
export function displayModal(modal) {
    $("#modal-container").css("display", "block");
    $("#modal-content").attr('src', $(modal).attr("data-full"));
    $("#modal-caption").html($(modal).attr("data-title"));
}