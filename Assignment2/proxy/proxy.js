const https = require('https');
const fs = require('fs');
var request = require("request");
var url = require('url');
const fetch = require("node-fetch");

//API keys
const nasaAPI_KEY = "uTFsb4n1GqvyMVunyzPZyzdUzvf1tEYJaWDPM3J2";
const weatherAPI_KEY = "94998cf875fdb8faea658f0d057cac8f";
const geocodeAPI_KEY = "f0e2b59a31a44bada81d707771e2e3b2";

//API request strings
const weatherQUERY = `http://api.openweathermap.org/data/2.5/weather?&appid=${weatherAPI_KEY}&units=metric&`;
const nasaQUERY = `https://api.nasa.gov/planetary/earth/imagery?dim=0.4&api_key=${nasaAPI_KEY}&`;
const geocodeQUERY = `https://api.opencagedata.com/geocode/v1/json?key=${geocodeAPI_KEY}&q=`

//Sets the secret keys for HTTPS
var options = {
    key: fs.readFileSync('./key.pem', 'utf8'),
    cert: fs.readFileSync('./cert.pem', 'utf8')
};

//creates server 
https.createServer(options, function (req, res) {
    //Loads URL parse variables
    let q = url.parse(req.url, true);
    lat = q.query.lat
    long = q.query.long
    date = q.query.date
    placeName = q.query.placename

    //If lat, long and date exist, API call must be for the nasa API 
    if (lat && long && date) {
        fetch(nasaQUERY + `lat=${lat}&lon=${long}&date=${date}`, {
          }).then(function(response) {
            res.writeHead(200, { 'Content-Type': 'application/json', "Access-Control-Allow-Origin": "*" });
            res.write(JSON.stringify({url: response.url}));
            res.end();
          });
    //If only lat and long, API call is for the weather query 
    } else if (lat && long) {
        request(weatherQUERY + `lat=${lat}&lon=${long}`, function (error, response, body) {
            res.writeHead(200, { 'Content-Type': 'application/json', "Access-Control-Allow-Origin": "*" });
            res.write(body);
            res.end();
        });
    //If placename exists, return the fowardGeocode request 
    } else if (placeName) {
        request(geocodeQUERY + placeName, function (error, response, body) {
            res.writeHead(200, { 'Content-Type': 'application/json', "Access-Control-Allow-Origin": "*" });
            res.write(body);
            res.end();
        });
    //If no URL variables, API call is invalid 
    } else {
        console.log("invalid request");
    }
}).listen(3001); 
