const API_KEY = "18e7885d1f7962a2c2fd7feef695e6b6"
const interestingRequest = "https://www.flickr.com/services/rest/?method=flickr.interestingness.getList&api_key=" + API_KEY + "&per_page=20&format=json&nojsoncallback=1";
const getSizeRequest = "https://www.flickr.com/services/rest/?method=flickr.photos.getSizes&api_key=" + API_KEY + "&format=json&nojsoncallback=1&photo_id=";
const searchRequest = `https://www.flickr.com/services/rest/?method=flickr.photos.search&api_key=${API_KEY}&sort=relevance&format=json&nojsoncallback=10&text=`;
let photos = [];
let nRequests = 0;
let nRecieved = 0;

$(document).ready(function () {
    $.get(interestingRequest, function (data) {
        fetchPhoto(data);
    });

    $("#search-button").click(seachButton);
});

function seachButton() {
    let userInput = $("#search-input").val();
    let searchQuery = searchRequest + userInput;
    
    if (userInput != "") {
        $.get(searchQuery, function (data) {
            nRequests = 10;
            for (let i = 0; i < 10; i++) {
                let photoObject = { "id": data.photos.photo[i].id, "title": data.photos.photo[i].title }
                photos.push(photoObject);
                getSizes(photoObject);
            }
        })
    }
}

function fetchPhoto(data){
    nRequests = 10;
    for (let i = 0; i < 10; i++) {
        let photoObject = {"id": data.photos.photo[i].id, "title": data.photos.photo[i].title}
        photos.push(photoObject);
        getSizes(photoObject);
    }
}

function getSizes(photoObject) {
    getSizeQuery = getSizeRequest + photoObject.id
    $.get(getSizeQuery, function(data){
         nRecieved++;
         photoObject.file = data.sizes.size[4].source;
         photoObject.full = data.sizes.size[data.sizes.size.length -1].source;
         if (nRequests === nRecieved) {
             display(photos);
             nRequests = 0;
             nRecieved = 0;
             photos = [];
         } 
    }); 
}

function display(photos) {
    let htmlStr = "";
    for (let i = 0; i < photos.length; i++) {
        htmlStr += `<figure><img src = "${photos[i].file}" alt="${photos[i].title}" height="200" width="200"><figcaption>${photos[i].title}</figcaption></figure>`;
    }
    $("#images-flex").html(htmlStr);

}

