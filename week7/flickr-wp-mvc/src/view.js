import $ from "jquery";

export function display(photos) {
    let htmlStr = "";
    for (let i = 0; i < photos.length; i++) {
        htmlStr += `<figure data-full="${photos[i].full}" data-title="${photos[i].title}"><img src = "${photos[i].file}" alt="${photos[i].title}" height="200" width="200"><figcaption>${photos[i].title}</figcaption></figure>`;
    }
    $("#images-flex").html(htmlStr);
}


