import $ from "jquery";
import styles from "../src/css/styles.css";
import image1 from "./assets/photos/DSC03810.jpg"
import image2 from "./assets/photos/DSC05750.jpg"
import { display } from "./view.js";
import * as flickr from "./flickr.js";



$(document).ready(function () {
    flickr.getInteresting(flickrReady); 
    $("#photo").html(`<img src=${image1}>`)
    $("#photo2").html(`<img src=${image2}>`)
    $("#search-button").click(seachButton);
    $("#modal-close").click(function(){
        $("#modal-container").css("display", "none");
        $("modal-content").attr("src","");
    });

});

function flickrReady(photos){
    display(photos);
    modalRegister();
}

function seachButton() {
    let userInput = $("#search-input").val();
    flickr.searchFlickr(userInput);
}

function modalRegister() {
    $('figure').each(function(index) {
        $(this).click(function(){
            $("#modal-container").css("display", "block");
            $("#modal-content").attr('src', $(this).attr("data-full"));
            $("#modal-caption").html($(this).attr("data-title"));
        })
    });
}

